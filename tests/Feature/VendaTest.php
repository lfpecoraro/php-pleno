<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Testing\Fluent\AssertableJson;
use Database\Seeders\VendedorSeeder;
use Tests\TestCase;

class VendaTest extends TestCase
{
  /** @test */
  public function pode_criar_venda()
  {
      $this->withoutExceptionHandling();

      $this->seed(VendedorSeeder::class);

      $valor = 100;
      $check_comissao = $valor * 0.065;

      $data = [
        'vendedor_id' => 1,
        'valor' => $valor
      ];
    
      $response = $this->withHeaders([
        'Content-Type' => 'application/json',
        ])->postJson('/api/venda', $data);
      $response->assertStatus(201)
      ->assertJson(fn (AssertableJson $json) =>
            $json->where('data.valor_venda', "$valor")
                 ->where('data.valor_comissao', "$check_comissao")
                 ->etc()
        );
  }
}
