<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class VendedorTest extends TestCase
{
    /** @test */
    public function pode_criar_vendedor()
  {
      $this->withoutExceptionHandling();
      $name = $this->faker->name;
      $email = $this->faker->email;
      $data = [
        'nome' => $name,
        'email' => $email
      ];
    
      $response = $this->withHeaders([
        'Content-Type' => 'application/json',
        ])->postJson('/api/vendedor', $data);
      $response->assertStatus(201)
      ->assertJson(fn (AssertableJson $json) =>
            $json->where('data.nome', "$name")
                 ->where('data.email', "$email")
                 ->etc()
        );
  }
}
