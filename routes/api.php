<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VendedorController;
use App\Http\Controllers\VendaController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('vendedores', [VendedorController::class, 'index']);
Route::post('vendedor', [VendedorController::class, 'store']);
Route::post('vendas', [VendedorController::class, 'show']);
Route::post('venda', [VendaController::class, 'store']);