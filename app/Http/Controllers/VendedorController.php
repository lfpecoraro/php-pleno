<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Models\Vendedor as Vendedor;
use App\Http\Resources\VendedorResource as VendedorResource;
use App\Http\Resources\VendedorComissaoResource as VendedorComissaoResource;
use App\Http\Resources\VendedorVendasResource as VendedorVendasResource;

class VendedorController extends Controller
{
    /*
        Retorna todos os vendedores com suas comissões acumuladas
    */
    public function index()
    {
        if (Cache::has('vendedores_index')) {
            $vendedores = Cache::get('vendedores_index');
        }
        else {
            $vendedores = Vendedor::all();
            $vendedores->loadSum('vendas', 'comissao');
            Cache::put('vendedores_index', $vendedores);
        }
        return VendedorComissaoResource::collection($vendedores);
    }

    /*
        Adiciona um novo vendedor
    */
    public function store(Request $request)
    {
        $vendedor = new Vendedor;
        $vendedor->nome = $request->input('nome');
        $vendedor->email = $request->input('email');

        if( $vendedor->save() ){
            Cache::forget('vendedores_index');
            return new VendedorResource($vendedor);
        }
    }

    /*
        Retorna um vendedor com suas respectivas vendas
    */
    public function show(Request $request)
    {
        if (Cache::has('vendedor_show_'.$request->input('id'))) {
            $vendedor = Cache::get('vendedor_show_'.$request->input('id'));
        }
        else {
            $vendedor = Vendedor::findOrFail( $request->input('id') );
            Cache::put('vendedor_show_'.$request->input('id'), $vendedor);
        }
        
        return new VendedorVendasResource( $vendedor );
    }
}
