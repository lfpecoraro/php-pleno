<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Models\Venda as Venda;
use App\Http\Resources\VendaResource as VendaResource;
use App\Http\Resources\VendaVendedorResource as VendaVendedorResource;

class VendaController extends Controller
{
    const TAXA_COMISSAO = 0.065;

    /*
        Registra uma venda para um vendedor
    */
    public function store(Request $request)
    {
        $venda = new Venda;
        $venda->vendedor_id = $request->input('vendedor_id');
        $venda->valor = $request->input('valor');
        $venda->data_venda = date("Y-m-d H:i:s");
        $venda->comissao = $this->_calcComissao($request->input('valor'));

        if( $venda->save() ){
            $venda->refresh();
            Cache::forget('vendedor_show_'.$request->input('vendedor_id'));
            return new VendaVendedorResource($venda);
        }
    }

    /*
        Calcula a comissão para o valor recebido
    */
    private function _calcComissao($valor) {
        return $valor * self::TAXA_COMISSAO;
    }

}
