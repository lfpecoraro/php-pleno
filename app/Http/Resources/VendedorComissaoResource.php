<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\VendaResource;

class VendedorComissaoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request){
        //return parent::toArray($request);
        return [
          'id' => $this->id,
          'nome' => $this->nome,
          'email' => $this->email,
          'comissao' => $this->vendas_sum_comissao ? $this->vendas_sum_comissao : "0.00"
        ];
    }
}
