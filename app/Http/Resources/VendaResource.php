<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\VendedorResource;

class VendaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'valor_venda' => $this->valor,
            'valor_comissao' => $this->comissao,
            'data_venda' => $this->data_venda
          ];
    }
}
