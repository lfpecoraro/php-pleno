<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\VendedorResource;

class VendaVendedorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'vendedor' => VendedorResource::make($this->vendedor),
            'valor_venda' => $this->valor,
            'valor_comissao' => $this->comissao,
            'data_venda' => $this->data_venda
          ];
    }
}
