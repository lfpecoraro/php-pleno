<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use App\Models\Venda;

class VendaSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $valor = mt_rand(1, 15000);
        $comissao = $valor * 0.065;
        Venda::create([
            'vendedor_id' => 1,
            'valor' => $valor,
            'comissao' => $comissao
        ]);
    }
}
