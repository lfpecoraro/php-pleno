<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendas', function (Blueprint $table) {
            $table->id();
            $table->decimal('valor', 19, 2);
            $table->decimal('comissao', 19, 2);
            $table->dateTime('data_venda');
            $table->foreignId('vendedor_id');
            $table->timestamps();
            $table->foreign('vendedor_id')->references('id')->on('vendedors');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendas');
    }
}
