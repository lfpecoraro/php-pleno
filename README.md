# Teste para Programador PHP Pleno [Luis Felipe Amaral Pecoraro] - Vortigo

Este projeto é uma API para registrar vendedores, vendas e calcular suas repectivas comissões

# Collection para teste no Postman

Na raiz do projeto existe um arquivo json (API Teste_PHP_Pleno.postman_collection.json) que contém a collection para facilitar os testes da API criada.

# Endpoints

## Cadastro de vendedor

- [POST] /api/vendedor  - Recebe os dados em Json para cadastrar um vendedor

Chamada 
<pre>
<code>
    {
        "nome": "Luis Peco",
        "email": "lfpecoraro@gmail.com"
    }
</code>
</pre>
Retorno
<pre>
<code>
    {
        "data": {
            "id": 10,
            "nome": "Luis Peco",
            "email": "lfpecoraro@gmail2.com"
        }
    }
</code>
</pre>

## Listagem de vendedores

- [GET] /api/vendedores - Lista todos os vendedores com suas comissões totais

Retorno
<pre>
<code>
    {
        "data": [
            {
                "id": 1,
                "nome": "Luis Peco",
                "email": "lfpecoraro@gmail.com",
                "comissao": "377.00"
            },
            {
                "id": 2,
                "nome": "Luis Peco",
                "email": "lfpecoraro2@gmail.com",
                "comissao": "52.00"
            },
            {
                "id": 3,
                "nome": "Luis Peco",
                "email": "lfpecoraro3@gmail.com",
                "comissao": "0.00"
            },
            {
                "id": 5,
                "nome": "Luis Peco",
                "email": "lfpecoraro4@gmail.com",
                "comissao": "0.00"
            },
            {
                "id": 6,
                "nome": "Luis Peco",
                "email": "lfpecoraro10@gmail.com",
                "comissao": "0.00"
            },
            {
                "id": 7,
                "nome": "Luis Peco",
                "email": "lfpecoraro12@gmail.com",
                "comissao": "0.00"
            },
            {
                "id": 8,
                "nome": "Luis Peco",
                "email": "lfpecoraro22@gmail.com",
                "comissao": "0.00"
            },
            {
                "id": 10,
                "nome": "Luis Peco",
                "email": "lfpecoraro@gmail2.com",
                "comissao": "0.00"
            }
        ]
    }
</code>
</pre>

## Cadastro de venda

- [POST] /api/venda     - Recebe os dados em Json para cadastrar uma venda para um vendedor

Chamada 
<pre>
<code>
    {
        "vendedor_id": "1",
        "valor": "200"
    }
</code>
</pre>

Retorno
<pre>
<code>
    {
        "data": {
            "vendedor": {
                "id": 1,
                "nome": "Luis Peco",
                "email": "lfpecoraro@gmail.com"
            },
            "valor_venda": "200.00",
            "valor_comissao": "13.00",
            "data_venda": "2021-05-28 16:27:36"
        }
    }
</code>
</pre>

## Listagem de vendas de um vendedor

- [POST] /api/vendas    - Retorna um vendedor com suas respectivas vendas

Chamada 
<pre>
<code>
    {
        "id": "1"
    }
</code>
</pre>

Retorno
<pre>
<code>
    {
        "data": {
            "id": 1,
            "nome": "Luis Peco",
            "email": "lfpecoraro@gmail.com",
            "vendas": [
                {
                    "valor_venda": "200.00",
                    "valor_comissao": "13.00",
                    "data_venda": "2021-05-27 22:22:45"
                },
                {
                    "valor_venda": "200.00",
                    "valor_comissao": "13.00",
                    "data_venda": "2021-05-27 22:38:34"
                },
                {
                    "valor_venda": "200.00",
                    "valor_comissao": "13.00",
                    "data_venda": "2021-05-27 22:38:49"
                },
                {
                    "valor_venda": "200.00",
                    "valor_comissao": "13.00",
                    "data_venda": "2021-05-27 22:38:59"
                }
            ]
        }
    }
</code>
</pre>